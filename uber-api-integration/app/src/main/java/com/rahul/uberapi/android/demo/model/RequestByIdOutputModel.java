package com.rahul.uberapi.android.demo.model;

/**
 * Created by razor on 23/7/15.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestByIdOutputModel {

    @Expose
    private String status;
    @Expose
    private Driver driver;
    @Expose
    private Integer eta;
    @Expose
    private Location location;
    @Expose
    private Vehicle vehicle;
    @SerializedName("surge_multiplier")
    @Expose
    private Double surgeMultiplier;
    @SerializedName("request_id")
    @Expose
    private String requestId;

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The driver
     */
    public Driver getDriver() {
        return driver;
    }

    /**
     *
     * @param driver
     * The driver
     */
    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    /**
     *
     * @return
     * The eta
     */
    public Integer getEta() {
        return eta;
    }

    /**
     *
     * @param eta
     * The eta
     */
    public void setEta(Integer eta) {
        this.eta = eta;
    }

    /**
     *
     * @return
     * The location
     */
    public Location getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     *
     * @param vehicle
     * The vehicle
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     *
     * @return
     * The surgeMultiplier
     */
    public Double getSurgeMultiplier() {
        return surgeMultiplier;
    }

    /**
     *
     * @param surgeMultiplier
     * The surge_multiplier
     */
    public void setSurgeMultiplier(Double surgeMultiplier) {
        this.surgeMultiplier = surgeMultiplier;
    }

    /**
     *
     * @return
     * The requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     *
     * @param requestId
     * The request_id
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

}
package com.rahul.uberapi.android.demo;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;

public class Constants {

    public static final boolean isProduction = false;
    public static final String ACCESS_TOKEN = "access_token";
    public static final String PRODUCT_ID = "product_id";
    public static final String UBER_REQUEST_STATUS_PROCESSING = "processing";
    public static final String UBER_REQUEST_STATUS_ACCEPTED = "accepted";
    public static final String UBER_REQUEST_STATUS_CANCELLED = "driver_canceled";
    public static final String UBER_REQUEST_STATUS_ARRIVING = "arriving";
    public static final String UBER_REQUEST_STATUS_PROGRESS = "in_progress";
    public static final String UBER_REQUEST_STATUS_COMPLETED = "completed";

    public static final String FETCHING_DRIVER = "Fetching Driver Details";
    public static final String CAB_ARRIVING = "Cab is arriving :)";
    private static HashMap<String, String> authParameters = new HashMap<String, String>();
    public static final String NO_TAXI = "Sorry, no cabs available";
    public static final String SOMETHING_WENT_WRONG = "Something went wrong";
    public static final String AUTHORIZE_URL = "https://login.uber.com/oauth/authorize";
    public static final String BASE_URL = "https://login.uber.com/";
    public static final String SCOPES = "profile history_lite history request";
    public static final String BASE_UBER_URL_V1 = "https://api.uber.com/v1/";
    public static final String BASE_UBER_URL_V1_1 = "https://api.uber.com/v1/";
    public static final double START_LATITUDE = 12.9000;
    public static final double START_LONGITUDE = 77.6300;
    public static final double END_LATITUDE = 12.9856;
    public static final double END_LONGITUDE = 77.6639;

    public static final String SERVER_TOKEN = "gajUw6Gltran2dgIjaqmnyn-n_2BBAdPEiUlbXF9";

    public static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();

    public static final String sharedPreference = "MyPrefs";
    public static final String accessToken = "accesstoken";

    public static final String REQUEST_ID = "request-id";
    public static final int TIME_DELAY = 4000;
    public static final String REQUEST_DRIVER = "Requesting Cab Driver...";

    public static String getUberClientId(Activity activity) {
        return getManifestData(activity, "com.rahul.uberapi.android.demo.UBER_CLIENT_ID");
    }

    public static String getUberClientSecret(Activity activity) {
        return getManifestData(activity, "com.rahul.uberapi.android.demo.UBER_CLIENT_SECRET");
    }

    public static String getUberRedirectUrl(Activity activity) {
        return getManifestData(activity, "com.rahul.uberapi.android.demo.UBER_REDIRECT_URL");
    }

    public static String getManifestData(Activity activity, String name) {
        String data = authParameters.get(name);
        if (data != null) {
            return data;
        }
        try {
            ApplicationInfo ai = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            data = bundle.getString(name);
            authParameters.put(name, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }
}

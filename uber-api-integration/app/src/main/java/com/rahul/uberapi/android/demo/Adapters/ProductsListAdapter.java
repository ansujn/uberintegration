package com.rahul.uberapi.android.demo.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rahul.uberapi.android.demo.R;
import com.rahul.uberapi.android.demo.holders.ProductHolder;
import com.rahul.uberapi.android.demo.model.Product;
import com.rahul.uberapi.android.demo.model.ProductList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuliza214 on 21/7/15.
 */
public class ProductsListAdapter extends ArrayAdapter<Product>{
    private Product mProduct;
    private ProductHolder holder;
    private ArrayList<Product> productList;
    private onItemInteractionListner mListner;
    private Dialog mDialog;
    public ProductsListAdapter(Context context, int resource,ArrayList<Product> list,onItemInteractionListner listner,Dialog dialog) {
        super(context, resource,list);
        this.productList = list;
        Log.v("inside cons",list.size()+"");
        mListner = listner;
        mDialog= dialog;
    }

    @Override
    public Product getItem(int position){
        return productList.get(position);
    }

    @Override
    public int getCount() {
        return productList.size();

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rootView = convertView;
        mProduct = getItem(position);
        holder =  new ProductHolder();
        if(rootView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product,parent,false);
            holder.tvProduct = (TextView) convertView.findViewById(R.id.tvProduct);
            holder.tvProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListner.onItemClick((String) getItem(position).getProductId());
                    mDialog.dismiss();
                }
            });
            convertView.setTag(holder);
        }
        else{
            holder = (ProductHolder) convertView.getTag();
        }
        holder.tvProduct.setText(mProduct.getDisplayName());
        return convertView;
    }
    public interface onItemInteractionListner{
        public void onItemClick(String name);
    }
}

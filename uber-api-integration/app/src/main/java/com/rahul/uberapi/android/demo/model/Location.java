package com.rahul.uberapi.android.demo.model;

import com.google.gson.annotations.Expose;
public class Location {

    @Expose
    private Double latitude;
    @Expose
    private Double longitude;
    @Expose
    private Integer bearing;

    /**
     *
     * @return
     * The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     * The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     * The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     *
     * @param longitude
     * The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     * The bearing
     */
    public Integer getBearing() {
        return bearing;
    }

    /**
     *
     * @param bearing
     * The bearing
     */
    public void setBearing(Integer bearing) {
        this.bearing = bearing;
    }

}
package com.rahul.uberapi.android.demo;

import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rahul.uberapi.android.demo.api.UberAPIClient;
import com.rahul.uberapi.android.demo.api.UberAPISandboxClient;
import com.rahul.uberapi.android.demo.api.UberCallback;
import com.rahul.uberapi.android.demo.model.RequestByIdOutputModel;
import com.rahul.uberapi.android.demo.model.RequestInputModel;
import com.rahul.uberapi.android.demo.utility.DownloadImageTask;
import com.rahul.uberapi.android.demo.utility.Utilities;

import org.w3c.dom.Text;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TrackArrivingTaxiActivity extends AppCompatActivity
        implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Marker mTaxi;
    private Marker mMyLocation;

    private int TIME_LAG = 1000;
    private int TAXI_ID = 1;
    private static Handler timerHandlerForApiCalls;
    private int COUNTER=0;
    private static final int MAX_COUNTER=5;
    private ImageView callImageView;
    private ImageView msgImageView;
    private Location mCurrentLocation;
    private LatLng mCurrentLatLng;
    private GoogleApiClient mGoogleApiClient;
    private Runnable runnable;
    private String requestId;
    private String accessToken;
    private RelativeLayout taxiDetails;
    private String productId;
    private GoogleMap googleMap;
    private TextView driverName;
    private TextView driverContacts;
    private TextView uberType;
    private TextView carSubType;
    private TextView carNumber;
    private CircleImageView driverImage;
    private ImageView carImage;

    //static data for TAXI moving GPS position
    private ArrayList<LatLng> latLngArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_arriving_taxi);
        Utilities.displayProgressDialog(TrackArrivingTaxiActivity.this, Constants.FETCHING_DRIVER, false);
        Bundle args = getIntent().getExtras();
        if(args != null){
            requestId = args.getString(Constants.REQUEST_ID);
            accessToken = args.getString(Constants.ACCESS_TOKEN);
            productId = Constants.isProduction? args.getString(Constants.PRODUCT_ID):null;
        }
        buildGoogleApiClient();
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        mCurrentLatLng = new LatLng(12.9000,77.6300);
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        viewInitialization();
    }

    private void viewInitialization() {
        taxiDetails = (RelativeLayout) findViewById(R.id.taxi_details);
        driverName = (TextView) findViewById(R.id.name);
        driverContacts = (TextView) findViewById(R.id.contacts);
        uberType = (TextView) findViewById(R.id.Uber_type);
        carSubType = (TextView) findViewById(R.id.car_sub_type);
        carNumber = (TextView) findViewById(R.id.car_number);
        carImage = (ImageView) findViewById(R.id.car_image);
        driverImage = (CircleImageView) findViewById(R.id.driver_image);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setMyLocationEnabled(true);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLatLng, 15));
        mMyLocation = googleMap.addMarker(new MarkerOptions()
                        .position(mCurrentLatLng)
                        .title("You")
                        .draggable(true)
        );
        runnable = new Runnable() {
            @Override
            public void run() {

                Log.v("timer", "inside timer");
            }
        };
        taxiDetailsApiCall();
    }

    private void taxiDetailsApiCall() {
        timerHandlerForApiCalls = new Handler();
        if(Constants.isProduction){
            runnable = new Runnable() {
                @Override
                public void run() {
                    callRequestStatusApiByIdProduction(requestId);
                }
            };
            callRequestStatusApiByIdProduction(requestId);
        }else {
            runnable = new Runnable() {
                @Override
                public void run() {
                    callRequstApiSandBox(productId);
                }
            };
            callRequstApiSandBox(productId);
        }
    }

    private void createDummyLatLngList() {
        latLngArrayList.add(new LatLng(12.9024870, 77.6350700));
        latLngArrayList.add(new LatLng(12.9024870, 77.6349610));
        latLngArrayList.add(new LatLng(12.9024870,77.6348510));
        latLngArrayList.add(new LatLng(12.9024870, 77.6347410));
        latLngArrayList.add(new LatLng(12.9024870, 77.6346310));
    }



    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, Utilities.createLocationRequest(), this);
    }

    @Override
    public void onLocationChanged(Location location) {
        changeMyLocation(location);
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.v("Google API Callback","Connection Done");
        changeMyLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.v("Google API Callback", "Connection Suspended");
        Log.v("Code", String.valueOf(i));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.v("Google API Callback", "Connection Failed");
        Log.v("Error Code", String.valueOf(connectionResult.getErrorCode()));
    }
    private void changeMyLocation(Location newLocation){
        mCurrentLocation = newLocation;
        mCurrentLatLng = new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
        mMyLocation.setPosition(mCurrentLatLng);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            Log.v("Google API","Updating Location");
            startLocationUpdates();
        }else {
            Log.v("Google API","Connecting");
            mGoogleApiClient.connect();
        }
        if (timerHandlerForApiCalls!=null){
            //resuming the api calls for setting taxi position
            timerHandlerForApiCalls.postDelayed(runnable, 3 * TIME_LAG);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("Google API", "Dis-connecting");
        mGoogleApiClient.disconnect();
        if ( mGoogleApiClient.isConnected()){
            stopLocationUpdates();
        }
        if (timerHandlerForApiCalls!=null){
            //pausing the api calls for setting taxi position
            timerHandlerForApiCalls.removeCallbacks(runnable);
        }
    }

    private void stopLocationUpdates(){
        Log.v("Google API", "Stopping Update Location");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void finish() {
        super.finish();
        timerHandlerForApiCalls.removeCallbacks(runnable);
    }
    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount()>=1){
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }
    }


    public void callRequestStatusApiByIdProduction(String requestId){
        timerHandlerForApiCalls.postDelayed(runnable,Constants.TIME_DELAY);
        UberAPIClient.getUberV1APIClient().getRequestStatusById(requestId,
                "Bearer " + accessToken,
                new Callback<RequestByIdOutputModel>() {
                    @Override
                    public void success(RequestByIdOutputModel requestByIdOutputModel, Response response) {
                        if(taxiDetails.getVisibility() != View.VISIBLE){
                            Utilities.cancelProgressDialog();
                            setVisibitlityDriverDeatils(requestByIdOutputModel);
                        }
                        updateTaxiMarker(requestByIdOutputModel.getLocation());
                    }
                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("track taxi",error.getMessage());
                    }
                }
        );
    }

    private void setVisibitlityDriverDeatils(final RequestByIdOutputModel requestByIdOutputModel) {
        DownloadImageTask driverImageTask = new DownloadImageTask(driverImage);
        driverImageTask.execute(requestByIdOutputModel.getDriver().getPictureUrl());
        DownloadImageTask taxiImageTask = new DownloadImageTask(carImage);
        taxiImageTask.execute(requestByIdOutputModel.getVehicle().getPictureUrl());
        carNumber.setText(requestByIdOutputModel.getVehicle().getLicensePlate());
        driverName.setText(requestByIdOutputModel.getDriver().getName());
        driverContacts.setText(requestByIdOutputModel.getDriver().getPhoneNumber());
        uberType.setText(requestByIdOutputModel.getVehicle().getMake());
        carSubType.setText(requestByIdOutputModel.getVehicle().getModel());
        taxiDetails.setVisibility(View.VISIBLE);
        driverContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                call(requestByIdOutputModel.getDriver().getPhoneNumber());
            }
        });
    }

    private void call(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

    private void callRequstApiSandBox(String productId){
        timerHandlerForApiCalls.postDelayed(runnable,Constants.TIME_DELAY);
        UberAPISandboxClient.getRequestClient().getRequest("Bearer " + accessToken, "application/json",
                new RequestInputModel(Constants.START_LATITUDE,
                        Constants.START_LONGITUDE,
                        Constants.END_LATITUDE,
                        Constants.END_LONGITUDE,
                        productId),
                new UberCallback<RequestByIdOutputModel>() {
                    @Override
                    public void success(RequestByIdOutputModel requestModel, Response response) {
                        if (taxiDetails.getVisibility() != View.VISIBLE) {
                            Utilities.cancelProgressDialog();
                            setVisibitlityDriverDeatils(requestModel);
                        }
                        updateTaxiMarker(requestModel.getLocation());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utilities.cancelProgressDialog();
                        Toast.makeText(TrackArrivingTaxiActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    private void updateTaxiMarker(com.rahul.uberapi.android.demo.model.Location location) {
        if (mTaxi == null){
            mTaxi = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(location.getLatitude(),location.getLongitude()))
                            .title("Taxi")
                            .draggable(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_icon))
            );
        }else {
            mTaxi.setPosition(new LatLng(location.getLatitude(),location.getLongitude()));
        }
    }
}

package com.rahul.uberapi.android.demo.utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.location.LocationRequest;


/**
 * Created by Luv_Gupta on 30-Jun-15.
 */
public class Utilities {

    private static ProgressDialog mProgressDialog;

    public static boolean isProgressDialogRunning() {
        return isProgressDialogRunning;
    }

    public static void setIsProgressDialogRunning(boolean isProgressDialogRunning) {
        Utilities.isProgressDialogRunning = isProgressDialogRunning;
    }

    private static boolean isProgressDialogRunning = false;


    // show progressing dialog
    public static void displayProgressDialog(Context context, String message, Boolean backButtonCancelable) {
        if (mProgressDialog == null && context != null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(message);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.show();
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(backButtonCancelable);
            setIsProgressDialogRunning(true);
        }
    }

    // hide progressing dialog
    public static void cancelProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog.cancel();
            mProgressDialog = null;
            setIsProgressDialogRunning(false);
        }
    }

    public static LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

}

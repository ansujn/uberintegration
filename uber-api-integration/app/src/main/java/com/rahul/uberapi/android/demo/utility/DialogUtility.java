package com.rahul.uberapi.android.demo.utility;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;

import com.rahul.uberapi.android.demo.Adapters.ProductsListAdapter;
import com.rahul.uberapi.android.demo.HomeScreenActivity;
import com.rahul.uberapi.android.demo.R;
import com.rahul.uberapi.android.demo.model.Product;
import com.rahul.uberapi.android.demo.model.ProductList;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by kuliza214 on 22/7/15.
 */
public class DialogUtility {
    private  ListView lvProducts ;
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public  void changeCabDialog(final Context context, final ArrayList<Product> productList , double width, double height, Boolean backButtonCancelable, Boolean touchOutsideCancelable){
        final Dialog dialog = new Dialog(context);
        //AlertDialog dialog = new AlertDialog.Builder(context).create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.products_popup);
        lvProducts = (ListView) dialog.findViewById(R.id.lvProducts);
        lvProducts.setAdapter(new ProductsListAdapter(context, 0, productList, (ProductsListAdapter.onItemInteractionListner) context,dialog));

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int DialogWidth = (int) (size.x * width);
        int DialogHeight = (int) (size.y * height);
        dialog.getWindow().setLayout(DialogWidth, DialogHeight);

        dialog.setCancelable(backButtonCancelable);
        dialog.setCanceledOnTouchOutside(touchOutsideCancelable);

        dialog.show();
    }
}

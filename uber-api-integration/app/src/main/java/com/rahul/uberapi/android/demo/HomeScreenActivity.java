package com.rahul.uberapi.android.demo;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.LevelListDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rahul.uberapi.android.demo.Adapters.ProductsListAdapter;
import com.rahul.uberapi.android.demo.api.UberAPIClient;
import com.rahul.uberapi.android.demo.api.UberAPISandboxClient;
import com.rahul.uberapi.android.demo.api.UberCallback;
import com.rahul.uberapi.android.demo.model.PriceEstimate;
import com.rahul.uberapi.android.demo.model.PriceEstimateList;
import com.rahul.uberapi.android.demo.model.Product;
import com.rahul.uberapi.android.demo.model.ProductList;
import com.rahul.uberapi.android.demo.model.RequestByIdInputModel;
import com.rahul.uberapi.android.demo.model.RequestByIdOutputModel;
import com.rahul.uberapi.android.demo.model.RequestModel;
import com.rahul.uberapi.android.demo.model.TimeEstimate;
import com.rahul.uberapi.android.demo.model.TimeEstimateList;
import com.rahul.uberapi.android.demo.model.RequestInputModel;
import com.rahul.uberapi.android.demo.utility.DialogUtility;
import com.rahul.uberapi.android.demo.utility.SharedPreferenceUtils;
import com.rahul.uberapi.android.demo.utility.Utilities;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.ResponseCallback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Luv_Gupta on 21-Jul-15.
 */
public class HomeScreenActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener,ProductsListAdapter.onItemInteractionListner{

    private static final String TAG="HomeScreenActivity";
    private GoogleMap googleMap;
    private String mAddress;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Location  mCurrentLocation;
    private double mLatitude;
    private double mLongitude;
    LocationRequest mLocationRequest;
    private boolean mRequestingLocationUpdates;
    private static Boolean flag=false;
    private String accessToken;
    private String tokenType;
    private ListView lvProducts;
    private ArrayList<Product> mProductsList;
    private Button btnChange,btnRequest;
    private DialogUtility dialogUtility;
    private RelativeLayout relativeLayout;
    private TextView tvProduct;
    private PriceEstimateList mPriceEstimateList;
    private TimeEstimateList mTimeEstimateList;
    private TextView estimatedTimeTextView;
    private TextView estimatedPriceTextView;
    private TextView mStartPoint,mEndPoint;
    private static Handler timerHandlerForApiCalls;
    private Runnable runnable;

    private Geocoder mGeoCoder;
    private List<Address> mStartAddresses;
    private List<Address> mEndAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homescreen);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        //setUpMapmarkers(mapFragment);
        buildGoogleApiClient();
        mapFragment.getMapAsync(this);
        accessToken = SharedPreferenceUtils.getInstance(HomeScreenActivity.this).getAccessToken();
        Log.e(TAG, "accessToken" + accessToken);
        //Getting Address from the LatLog
        mGeoCoder = new Geocoder(this,Locale.getDefault());
        mStartPoint = (TextView) findViewById(R.id.tvStartingPoint);
        mEndPoint = (TextView) findViewById(R.id.tvFinalPoint);
        try {
            mStartAddresses = mGeoCoder.getFromLocation(Constants.START_LATITUDE,Constants.START_LONGITUDE,1);
            mEndAddress = mGeoCoder.getFromLocation(Constants.END_LATITUDE,Constants.END_LONGITUDE,1);
            mStartPoint.setText(mStartAddresses.get(0).getAddressLine(0));
            mEndPoint.setText(mEndAddress.get(0).getAddressLine(0));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        btnChange = (Button) findViewById(R.id.btnChange);
        relativeLayout = (RelativeLayout) findViewById(R.id.rlChange);
        tvProduct = (TextView)findViewById(R.id.tvCabName);
        btnRequest = (Button)findViewById(R.id.btnRequest);
        estimatedPriceTextView = (TextView) findViewById(R.id.rvPrice);
        estimatedTimeTextView = (TextView) findViewById(R.id.tvPickUpTimeIn);
        Utilities.displayProgressDialog(this, "Getting Products", false);
        getUberProductsApiCall();
        createLocationRequest();
        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String producdId = (String) (v).getTag();
                Utilities.displayProgressDialog(HomeScreenActivity.this, "Requesting Cab", false);
                callRequestAPI(producdId);
            }
        });
    }

    private void setUpMapmarkers(MapFragment mapFragment) {
        if(googleMap == null){
            googleMap = mapFragment.getMap();
        }
        googleMap.clear();
        googleMap.addMarker(new MarkerOptions().position(new LatLng(Constants.START_LATITUDE,Constants.START_LONGITUDE))).setIcon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
        //googleMap.addMarker(new MarkerOptions().position(new LatLng(Constants.START_LATITUDE,Constants.START_LONGITUDE))).setTitle("Marker");
        googleMap.addMarker(new MarkerOptions().position(new LatLng(Constants.END_LATITUDE,Constants.END_LONGITUDE))).setTitle("Marker");
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Constants.START_LATITUDE,Constants.START_LONGITUDE), 15.0f));

    }

    private void setVisibilityOfView(String productId) {
        tvProduct.setText(getProductNameFromList(productId));
        estimatedTimeTextView.setText("Pickup in " + String.valueOf(getMinutes(getEstimatedTimeByProductId(productId))) + " mins.");
        estimatedPriceTextView.setText("$ " + getEstimatedPriceByProductId(productId));
        if(relativeLayout.getVisibility()!= View.VISIBLE){
            relativeLayout.setVisibility(View.VISIBLE);
            btnRequest.setVisibility(View.VISIBLE);
        }
        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mProductsList = new ArrayList<Product>();
                dialogUtility = new DialogUtility();
                dialogUtility.changeCabDialog(HomeScreenActivity.this, mProductsList, 0.6, 0.5, true, true);
            }
        });
        btnRequest.setTag(productId);
    }

    private String getProductNameFromList(String productId) {
        for (Product product:mProductsList){
            if (product.getProductId().equals(productId)){
                return product.getDisplayName();
            }
        }
        return null;
    }

    private String getEstimatedPriceByProductId(String productId){
        for (PriceEstimate priceEstimate:mPriceEstimateList.getPrices()){
            if(priceEstimate.getProductId().equals(productId))
                return priceEstimate.getEstimate();
        }
        return null;
    }

    private int getEstimatedTimeByProductId(String productId){
        for (TimeEstimate timeEstimate:mTimeEstimateList.getTimes()){
            if (timeEstimate.getProductId().equals(productId))
                return timeEstimate.getEstimate();
        }
        return 0;
    }

    private double getMinutes(int duration){
        return duration/60;
    }

    private void getUberProductsApiCall() {
        UberAPIClient.getUberV1APIClient().getProducts(accessToken, Constants.SERVER_TOKEN, Constants.START_LATITUDE, Constants.START_LONGITUDE,
                new UberCallback<ProductList>() {
                    @Override
                    public void success(ProductList productList, Response response) {
                        mProductsList = (ArrayList) productList.getProducts();
                        if (mProductsList.size() > 0) {
                            getPriceApi();
                        }
                        else {
                            Toast.makeText(HomeScreenActivity.this, Constants.NO_TAXI, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utilities.cancelProgressDialog();
                        Toast.makeText(HomeScreenActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getProductEstimateTime(String productId){
        UberAPIClient.getUberV1APIClient().getTimeEstimates(accessToken, Constants.SERVER_TOKEN, Constants.START_LATITUDE, Constants.START_LONGITUDE,
                new UberCallback<TimeEstimateList>() {
                    @Override
                    public void success(TimeEstimateList timeEstimateList, Response response) {
                        Utilities.cancelProgressDialog();
                        mTimeEstimateList = timeEstimateList;
                        setVisibilityOfView(mProductsList.get(0).getProductId());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utilities.cancelProgressDialog();
                        Log.v("ERROR", error.getMessage());
                    }
                }
        );
    }

    public void callRequestAPI(final String productId){
        timerHandlerForApiCalls = new Handler();
        if(Constants.isProduction){
            callReqestApiProduction(productId);
        }
        else {
            runnable = new Runnable() {
                @Override
                public void run() {
                    callRequstApiSandBox(productId);
                }
            };
            callRequstApiSandBox(productId);
        }
    }

    private void callReqestApiProduction(String productId){
        RequestInputModel requestInputModel = new RequestInputModel(Constants.START_LATITUDE,Constants.START_LONGITUDE,Constants.END_LATITUDE,Constants.END_LONGITUDE,productId);
        UberAPIClient.getUberV1APIClient().getRequest("Bearer " + accessToken, "application/json",
                requestInputModel,
                new Callback<RequestByIdOutputModel>() {
                    @Override
                    public void success(final RequestByIdOutputModel requestByIdOutputModel, Response response) {
                        if (requestByIdOutputModel.getStatus().equals("processing")) {
                            runnable = new Runnable() {
                                @Override
                                public void run() {
                                    callRequestStatusApiByIdProduction(requestByIdOutputModel.getRequestId());
                                }
                            };
                            if (!Utilities.isProgressDialogRunning()) {
                                Utilities.displayProgressDialog(HomeScreenActivity.this, "Finding Cab Driver", false);
                            }
                        } else if (requestByIdOutputModel.getStatus().equals("accepted")) {
                            Intent intent = new Intent(HomeScreenActivity.this, TrackArrivingTaxiActivity.class);
                            intent.putExtra(Constants.REQUEST_ID, requestByIdOutputModel.getRequestId());
                            intent.putExtra(Constants.ACCESS_TOKEN,accessToken);
                            Utilities.cancelProgressDialog();
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (Utilities.isProgressDialogRunning()) {
                            Utilities.cancelProgressDialog();
                        }
                        Toast.makeText(HomeScreenActivity.this, Constants.SOMETHING_WENT_WRONG, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    public void callRequestStatusApiByIdProduction(String requestId){
        UberAPIClient.getUberV1APIClient().getRequestStatusById(requestId,
                "Bearer " + accessToken,
                new Callback<RequestByIdOutputModel>() {
                    @Override
                    public void success(RequestByIdOutputModel requestByIdOutputModel, Response response) {
                        if (requestByIdOutputModel.getStatus().equals("accepted")) {
                            timerHandlerForApiCalls.removeCallbacks(runnable);
                            Intent intent = new Intent(HomeScreenActivity.this, TrackArrivingTaxiActivity.class);
                            intent.putExtra(Constants.REQUEST_ID, requestByIdOutputModel.getRequestId());
                            intent.putExtra(Constants.ACCESS_TOKEN, accessToken);
                            Utilities.cancelProgressDialog();
                            startActivity(intent);
                        } else {
                            timerHandlerForApiCalls.postDelayed(runnable, Constants.TIME_DELAY);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(Constants.REQUEST_ID, error.getMessage());
                    }
                }
        );
    }

    private void callRequstApiSandBox(final String productId){
        UberAPISandboxClient.getRequestClient().getRequest("Bearer " + accessToken, "application/json",
                new RequestInputModel(Constants.START_LATITUDE,
                        Constants.START_LONGITUDE,
                        Constants.END_LATITUDE,
                        Constants.END_LONGITUDE,
                        productId),
                new UberCallback<RequestByIdOutputModel>() {
                    @Override
                    public void success(RequestByIdOutputModel requestModel, Response response) {
                        if (requestModel.getStatus().equals("processing")) {
                            Utilities.cancelProgressDialog();
                            Utilities.displayProgressDialog(HomeScreenActivity.this, Constants.REQUEST_DRIVER, false);
                            timerHandlerForApiCalls.postDelayed(runnable, Constants.TIME_DELAY);
                        } else if (requestModel.getStatus().equals("accepted")) {
                            timerHandlerForApiCalls.removeCallbacks(runnable);
                            Intent intent = new Intent(HomeScreenActivity.this, TrackArrivingTaxiActivity.class);
                            intent.putExtra(Constants.REQUEST_ID, requestModel.getRequestId());
                            intent.putExtra(Constants.ACCESS_TOKEN, accessToken);
                            intent.putExtra(Constants.PRODUCT_ID, productId);
                            Utilities.cancelProgressDialog();
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utilities.cancelProgressDialog();
                        Toast.makeText(HomeScreenActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }
    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        mGoogleApiClient.disconnect();
        if ( mGoogleApiClient.isConnected()){
            //stopLocationUpdates();
        }

    }

    protected void stopLocationUpdates() {
        mRequestingLocationUpdates = false;
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mRequestingLocationUpdates = true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap=googleMap;
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        //Current location will be same as last location for the first time.
        mCurrentLocation = mLastLocation;
        if (mLastLocation != null) {
            mLongitude = mLastLocation.getLongitude();
            mLatitude = mLastLocation.getLatitude();

            getUIUpdate(mLastLocation);

        }

        startLocationUpdates();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //Saving last location for Fututre Use....
        mLastLocation = mCurrentLocation;
        //Updating current location
        mCurrentLocation = location;
        mLatitude= location.getLatitude();
        mLongitude = location.getLongitude();
        getUIUpdate(mCurrentLocation);
    }


    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void getUIUpdate(Location location){
        final LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
        BitmapDrawable bmd = (BitmapDrawable) getResources().getDrawable(R.drawable.pin,null).getCurrent();
        Bitmap b = bmd.getBitmap();
        Bitmap bmHalfSize = Bitmap.createScaledBitmap(b,b.getWidth()/2,b.getHeight()/2,false);
      //  if(flag!=true) {
            googleMap.clear();
            googleMap.addMarker(new MarkerOptions().position(loc)).setIcon(BitmapDescriptorFactory.fromBitmap(bmHalfSize));
            googleMap.addMarker(new MarkerOptions().position(new LatLng(Constants.END_LATITUDE,Constants.END_LONGITUDE))).setTitle("circle");
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 15.0f));
            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    // render page
                }
            });
            try {
                Geocoder gcd = new Geocoder(HomeScreenActivity.this, Locale.getDefault());
                List<Address> addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                ////   if (!flag)
                //   mLocation.setText(addresses.get(0).getSubLocality() + " " + addresses.get(0).getLocality() + ", " + addresses.get(0).getCountryName());
//            if (addresses.size() > 0)
//                mEditText.setText(addresses.get(0).getSubLocality()+" "+addresses.get(0).getLocality()+", "+addresses.get(0).getCountryName());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
       // }

    }

    public void setDestinationUiUpdate(com.rahul.uberapi.android.demo.model.Location location){
        //Set Destination UI Updates
    }
    public void getPriceApi() {
        UberAPIClient.getUberV1APIClient().getPriceEstimates(accessToken, Constants.SERVER_TOKEN, Constants.START_LATITUDE, Constants.START_LONGITUDE, Constants.END_LATITUDE, Constants.END_LONGITUDE,
                new UberCallback<PriceEstimateList>() {
                    @Override
                    public void success(PriceEstimateList priceEstimateList, Response response) {
                        mPriceEstimateList = priceEstimateList;
                        if(mPriceEstimateList.getPrices().size()>0 && priceEstimateListHasProduct(mProductsList.get(0).getProductId())){
                            getProductEstimateTime(mProductsList.get(0).getProductId());
                        }
                        else {
                            Toast.makeText(HomeScreenActivity.this,Constants.SOMETHING_WENT_WRONG,Toast.LENGTH_SHORT);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Utilities.cancelProgressDialog();
                        Log.e("failure", error.getResponse() + error.getMessage());

                    }
                });
    }

    private boolean priceEstimateListHasProduct(String estimateByProductId) {
        boolean result = false;
        for (PriceEstimate priceEstimate:mPriceEstimateList.getPrices()){
            if (priceEstimate.getProductId().equals(estimateByProductId))
                return true;
        }
        return result;
    }

    @Override
    public void onItemClick(String productId){
        Log.e("Product Id ",productId+" ");
        setVisibilityOfView(productId);
    }
}
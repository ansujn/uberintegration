package com.rahul.uberapi.android.demo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestInputModel {

    public RequestInputModel(Double startLatitude, Double startLongitude, Double endLatitude, Double endLongitude, String productId) {
        this.startLatitude = startLatitude;
        this.startLongitude = startLongitude;
        this.endLatitude = endLatitude;
        this.endLongitude = endLongitude;
        this.productId = productId;
    }

    @SerializedName("start_latitude")
    @Expose
    private Double startLatitude;
    @SerializedName("start_longitude")
    @Expose
    private Double startLongitude;
    @SerializedName("end_latitude")
    @Expose
    private Double endLatitude;
    @SerializedName("end_longitude")
    @Expose
    private Double endLongitude;
    @SerializedName("product_id")
    @Expose
    private String productId;

    /**
     *
     * @return
     * The startLatitude
     */
    public Double getStartLatitude() {
        return startLatitude;
    }

    /**
     *
     * @param startLatitude
     * The start_latitude
     */
    public void setStartLatitude(Double startLatitude) {
        this.startLatitude = startLatitude;
    }

    /**
     *
     * @return
     * The startLongitude
     */
    public Double getStartLongitude() {
        return startLongitude;
    }

    /**
     *
     * @param startLongitude
     * The start_longitude
     */
    public void setStartLongitude(Double startLongitude) {
        this.startLongitude = startLongitude;
    }

    /**
     *
     * @return
     * The endLatitude
     */
    public Double getEndLatitude() {
        return endLatitude;
    }

    /**
     *
     * @param endLatitude
     * The end_latitude
     */
    public void setEndLatitude(Double endLatitude) {
        this.endLatitude = endLatitude;
    }

    /**
     *
     * @return
     * The endLongitude
     */
    public Double getEndLongitude() {
        return endLongitude;
    }

    /**
     *
     * @param endLongitude
     * The end_longitude
     */
    public void setEndLongitude(Double endLongitude) {
        this.endLongitude = endLongitude;
    }

    /**
     *
     * @return
     * The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     *
     * @param productId
     * The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

}
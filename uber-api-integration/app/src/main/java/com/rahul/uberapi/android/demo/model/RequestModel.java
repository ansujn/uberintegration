package com.rahul.uberapi.android.demo.model;

/**
 * Created by Luv_Gupta on 21-Jul-15.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestModel {

    @SerializedName("request_id")
    @Expose
    private String requestId;
    @Expose
    private String status;
    @Expose
    private Object vehicle;
    @Expose
    private Object driver;
    @Expose
    private Object location;
    @Expose
    private Integer eta;
    @SerializedName("surge_multiplier")
    @Expose
    private Object surgeMultiplier;

    /**
     *
     * @return
     * The requestId
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     *
     * @param requestId
     * The request_id
     */
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The vehicle
     */
    public Object getVehicle() {
        return vehicle;
    }

    /**
     *
     * @param vehicle
     * The vehicle
     */
    public void setVehicle(Object vehicle) {
        this.vehicle = vehicle;
    }

    /**
     *
     * @return
     * The driver
     */
    public Object getDriver() {
        return driver;
    }

    /**
     *
     * @param driver
     * The driver
     */
    public void setDriver(Object driver) {
        this.driver = driver;
    }

    /**
     *
     * @return
     * The location
     */
    public Object getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(Object location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The eta
     */
    public Integer getEta() {
        return eta;
    }

    /**
     *
     * @param eta
     * The eta
     */
    public void setEta(Integer eta) {
        this.eta = eta;
    }

    /**
     *
     * @return
     * The surgeMultiplier
     */
    public Object getSurgeMultiplier() {
        return surgeMultiplier;
    }

    /**
     *
     * @param surgeMultiplier
     * The surge_multiplier
     */
    public void setSurgeMultiplier(Object surgeMultiplier) {
        this.surgeMultiplier = surgeMultiplier;
    }

}
package com.rahul.uberapi.android.demo.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.Double;
import java.lang.Object;
import java.lang.String;

        import java.util.ArrayList;
        import java.util.List;

public class PriceDetails {

    @SerializedName("distance_unit")
    @Expose
    private String distanceUnit;
    @SerializedName("cost_per_minute")
    @Expose
    private Double costPerMinute;
    @SerializedName("service_fees")
    @Expose
    private List<Object> serviceFees = new ArrayList<Object>();
    @Expose
    private Double minimum;
    @SerializedName("cost_per_distance")
    @Expose
    private Double costPerDistance;
    @Expose
    private Double base;
    @SerializedName("cancellation_fee")
    @Expose
    private Double cancellationFee;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;

    /**
     *
     * @return
     * The distanceUnit
     */
    public String getDistanceUnit() {
        return distanceUnit;
    }

    /**
     *
     * @param distanceUnit
     * The distance_unit
     */
    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    /**
     *
     * @return
     * The costPerMinute
     */
    public Double getCostPerMinute() {
        return costPerMinute;
    }

    /**
     *
     * @param costPerMinute
     * The cost_per_minute
     */
    public void setCostPerMinute(Double costPerMinute) {
        this.costPerMinute = costPerMinute;
    }

    /**
     *
     * @return
     * The serviceFees
     */
    public List<Object> getServiceFees() {
        return serviceFees;
    }

    /**
     *
     * @param serviceFees
     * The service_fees
     */
    public void setServiceFees(List<Object> serviceFees) {
        this.serviceFees = serviceFees;
    }

    /**
     *
     * @return
     * The minimum
     */
    public Double getMinimum() {
        return minimum;
    }

    /**
     *
     * @param minimum
     * The minimum
     */
    public void setMinimum(Double minimum) {
        this.minimum = minimum;
    }

    /**
     *
     * @return
     * The costPerDistance
     */
    public Double getCostPerDistance() {
        return costPerDistance;
    }

    /**
     *
     * @param costPerDistance
     * The cost_per_distance
     */
    public void setCostPerDistance(Double costPerDistance) {
        this.costPerDistance = costPerDistance;
    }

    /**
     *
     * @return
     * The base
     */
    public Double getBase() {
        return base;
    }

    /**
     *
     * @param base
     * The base
     */
    public void setBase(Double base) {
        this.base = base;
    }

    /**
     *
     * @return
     * The cancellationFee
     */
    public Double getCancellationFee() {
        return cancellationFee;
    }

    /**
     *
     * @param cancellationFee
     * The cancellation_fee
     */
    public void setCancellationFee(Double cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    /**
     *
     * @return
     * The currencyCode
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     *
     * @param currencyCode
     * The currency_code
     */
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

}
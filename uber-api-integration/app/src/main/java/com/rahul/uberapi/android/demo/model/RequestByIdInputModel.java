package com.rahul.uberapi.android.demo.model;

/**
 * Created by razor on 23/7/15.
 */

import com.google.gson.annotations.Expose;


public class RequestByIdInputModel {

    public RequestByIdInputModel(String status) {
        this.status = status;
    }

    @Expose
    private String status;

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

}

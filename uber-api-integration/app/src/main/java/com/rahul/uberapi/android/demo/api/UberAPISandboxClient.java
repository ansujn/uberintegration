package com.rahul.uberapi.android.demo.api;

import com.rahul.uberapi.android.demo.BuildConfig;
import com.rahul.uberapi.android.demo.Constants;
import com.rahul.uberapi.android.demo.model.RequestByIdInputModel;
import com.rahul.uberapi.android.demo.model.RequestByIdOutputModel;
import com.rahul.uberapi.android.demo.model.RequestInputModel;
import com.rahul.uberapi.android.demo.model.RequestModel;

import retrofit.Callback;
import retrofit.Endpoint;
import retrofit.ResponseCallback;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.EncodedPath;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * Created by razor on 23/7/15.
 */
public class UberAPISandboxClient {

    private static UberAPISandboxInterface sUberAPIService;
    private static UberSandboxEndPoint sEndPoint = new UberSandboxEndPoint(Constants.BASE_UBER_URL_V1, Constants.BASE_UBER_URL_V1_1);


    public static UberAPISandboxInterface getRequestClient() {
        if (sUberAPIService == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("https://sandbox-api.uber.com/v1")
                    .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                    .build();

            sUberAPIService = restAdapter.create(UberAPISandboxInterface.class);
        }

        return sUberAPIService;
    }

    public interface UberAPISandboxInterface{

        /**
         * The Price Estimates endpoint returns an estimated price range for each product offered
         * at a given location. The price estimate is provided as a formatted string with the full
         * price range and the localized currency symbol.
         * <p/>
         * The response also includes low and high estimates, and the ISO 4217 currency code for
         * situations requiring currency conversion. When surge is active for a particular product,
         * its surge_multiplier will be greater than 1, but the price estimate already factors in
         * this multiplier.
         *
         * @param authToken      OAuth 2.0 bearer token or server_token
        //         * @param startLatitude  Latitude component of start location.
        //         * @param startLongitude Longitude component of start location.
        //         * @param endLatitude    Longitude component of start location.
        //         * @param endLongitude   Longitude component of end location.
         * @param callback
         */



        @POST("/requests")
        void getRequest( @Header("Authorization") String authToken,
                         @Header("content-type") String content,
                         @Body RequestInputModel requestInputModel,
                         Callback<RequestByIdOutputModel> callback);


        @PUT("/sandbox/requests/{request_id}")
        void getRequestStatusById(@EncodedPath("request_id") String requestId,
                                  @Header("content-type") String content,
                                  @Header("Authorization") String authToken,
                                  @Body RequestByIdInputModel requestByIdInputModel,
                                  ResponseCallback responseCallback);

    }

    private static class UberSandboxEndPoint implements Endpoint {

        private final String apiUrlV1, apiUrlV11;
        private boolean useV11 = false;

        private UberSandboxEndPoint(String apiUrlV1, String apiUrlV11) {
            this.apiUrlV1 = apiUrlV1;
            this.apiUrlV11 = apiUrlV11;
        }

        public void setVersion(boolean useV11) {
            this.useV11 = useV11;
        }

        @Override
        public String getUrl() {
            return useV11 ? apiUrlV11 : apiUrlV1;
        }

        @Override
        public String getName() {
            return "default";
        }
    }
}

package com.rahul.uberapi.android.demo.model;

/**
 * Created by razor on 23/7/15.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Vehicle {

    @Expose
    private String make;
    @Expose
    private String model;
    @SerializedName("license_plate")
    @Expose
    private String licensePlate;
    @SerializedName("picture_url")
    @Expose
    private String pictureUrl;

    /**
     *
     * @return
     * The make
     */
    public String getMake() {
        return make;
    }

    /**
     *
     * @param make
     * The make
     */
    public void setMake(String make) {
        this.make = make;
    }

    /**
     *
     * @return
     * The model
     */
    public String getModel() {
        return model;
    }

    /**
     *
     * @param model
     * The model
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     *
     * @return
     * The licensePlate
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     *
     * @param licensePlate
     * The license_plate
     */
    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    /**
     *
     * @return
     * The pictureUrl
     */
    public String getPictureUrl() {
        return pictureUrl;
    }

    /**
     *
     * @param pictureUrl
     * The picture_url
     */
    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

}
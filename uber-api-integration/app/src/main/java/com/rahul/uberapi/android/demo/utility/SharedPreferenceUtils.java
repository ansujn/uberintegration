package com.rahul.uberapi.android.demo.utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.rahul.uberapi.android.demo.Constants;

/**
 * Created by Luv_Gupta on 21-Jul-15.
 */
public class SharedPreferenceUtils {
    private static SharedPreferenceUtils sInstance;

    private SharedPreferences mPrefs;

    public static SharedPreferenceUtils getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPreferenceUtils(context);
        }
        return sInstance;
    }

    private SharedPreferenceUtils(Context context) {
        mPrefs = context.getSharedPreferences(Constants.sharedPreference, 0);
    }

    public void setAccessToken(String accessToken) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(Constants.accessToken, accessToken);
        editor.commit();
    }

    public String getAccessToken() {
        return mPrefs.getString(Constants.accessToken, null);
    }
}
